const express = require('express'); // Initialize express router
const People = require('../models/people');
const router = express.Router();


// Set default API response
router.get('/', (req, res) => {
    res.json({
        status: 'Whooa! container solutions Exercise API Works!!!',
        message: 'You can go ahead and test other endpoints,'
    });
});

//get all userdatas
router.get('/people', async (req, res) => {
        try {
            const users = await People.find();
            return res.status(200).json({
                users
            });
    
        } catch (e) {
            console.log(e);
        }
    });
    
//view single userdata
router.get('/people/:id', async (req, res) => {
    try {
       const person = await People.find({_id: req.params.id});
       if (person.length === 0){
           return res.status (404).json({msg: "user not found"})
        }
          res.status(200).json({msg: "user found", data: person});
    } catch (e) {
        console.log(e);
    }
});

//delete single userdata
router.delete('/people/:id', async (req, res) => {
    try {
        await People.findOneAndDelete({_id: req.params.id});

        res.status(200).json({msg: "user deleted"});
    } catch (e) {
        console.log(e);
    }
});

//update single userdata
router.put('/people/:id', async (req, res) => {
    try {
       
        await People.findOneAndUpdate({_id: req.params.id} , req.body, {new :true });
                
        res.status(200).json({msg: "user updated"});
    } catch (e) {
        console.log(e);
    }
});

//create new userdata
router.post('/people', async (req, res) => {
    try {
        const user = await People(req.body);
        user.save();

        return res.status(201).json({ user });

    } catch (e) {
        console.log(e);
    }
});


// Export API routes
module.exports = router;