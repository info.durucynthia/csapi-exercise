const express = require ('express');
const apiRoutes = require('./src/api-routes'); // Import routes
const mongoose = require ('mongoose');
const dotenv = require('dotenv');
const { mongo_db_url } = require("./config")
const app = express(); 
const path = require('path')

const PORT = process.env.port || 3000; // check for env variable port or use the specified port

// Connect to MongDB server
mongoose.connect( mongo_db_url, { useNewUrlParser: true, useUnifiedTopology: true});

// middlewares
app.use(express.json());
app.use(apiRoutes);  // Use Api routes in the App

app.listen (PORT );



