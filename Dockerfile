
FROM node:alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package.json  .

RUN npm install

RUN npm install dotenv

COPY . .

EXPOSE 8000

CMD "npm" "start"

