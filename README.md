# From Exercise Instructions 

- [x] I created a DB using a Cloud database service(mongo atlas) and created a collection from titanic.csv file 
- [x] I created an API using Node/Express Js,using http requests with the following endpoints:

GET /people,
POST /people,
GET /people/:id,
DELETE /people/:id,
PUT /people/:id

- [x] Containerised Application, created a docker image ceceteras/csapp:5.0, pushed the image to docker hub
- [x] Deployed to K8s (Minikube).


# Prerequisites

- A macOS or Linux operating system 
- Docker
- Minikube. Install minikube https://minikube.sigs.k8s.io/docs/start/

# Setup/Run the Application
<!-- Run the shell script  -->
./run.sh

<!-- Make sure you docker and minikube is running -->


