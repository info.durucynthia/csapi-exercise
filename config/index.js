require('dotenv').config();

module.exports = {
    mongo_db_url: process.env.MONGODB_URL
}