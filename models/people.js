const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const peopleSchema = new Schema({

    survived: {type: Number},
    Pclass: { type:Number},
    Name: {type:String},
    Sex: {type:String},
    Age: { type: Number},
    "Siblings/Spouses Aboard" : {type:Number},
    "Parents/Children Aboard" : {type:Number},
    Fare : {},

}, {
    timestamps: true
});

const People = mongoose.model('userdatas', peopleSchema);

module.exports = People;